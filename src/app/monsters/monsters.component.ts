import { Component, OnInit } from "@angular/core";
import { DataService } from "../data.service";
import { Monster } from "../monster.interface";

@Component({
  selector: "app-monsters",
  templateUrl: "./monsters.component.html",
  styleUrls: ["./monsters.component.css"]
})
export class MonstersComponent implements OnInit {
  monsters: Monster[];

  constructor(private dataService: DataService) {}

  ngOnInit(): void {
    this.monsters = this.dataService.monsters;
  }

  somethingChanged(event) {
    console.log(event.target.value);
  }
}
