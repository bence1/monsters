import { Component, OnInit, Input, Output } from "@angular/core";

import { Monster } from "../../monster.interface";
import { DataService } from "src/app/data.service";

@Component({
  selector: "app-monster",
  templateUrl: "./monster.component.html",
  styleUrls: ["./monster.component.css"]
})
export class MonsterComponent implements OnInit {
  @Input() monster: Monster;

  constructor(private dataService: DataService) {}

  ngOnInit(): void {}

  onRemoveMonster(monster: Monster) {
    this.dataService.removeMonster(monster);
  }
}
