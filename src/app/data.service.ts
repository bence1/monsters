import { Injectable } from "@angular/core";

@Injectable()
export class DataService {
  monsters = [
    {
      name: "Rammus",
      img: "../assets/icons/defense.svg",
      attack: 4,
      defense: 9
    },
    {
      name: "Kassadin",
      img: "../assets/icons/water.svg",
      attack: 7,
      defense: 4
    },
    {
      name: "Ahri",
      img: "../assets/icons/fire.svg",
      attack: 8,
      defense: 3
    }
  ];

  constructor() {}

  createMonster(name: string, img: string, attack: number, defense: number) {
    this.monsters.push({
      name: name,
      img: img,
      attack: attack,
      defense: defense
    });
  }

  removeMonster(monster) {
    this.monsters.splice(this.monsters.indexOf(monster), 1);
  }
}
