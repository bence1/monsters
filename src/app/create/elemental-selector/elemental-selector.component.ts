import { Component, OnInit } from "@angular/core";
import { ThrowStmt } from "@angular/compiler";

@Component({
  selector: "app-elemental-selector",
  templateUrl: "./elemental-selector.component.html",
  styleUrls: ["./elemental-selector.component.css"]
})
export class ElementalSelectorComponent implements OnInit {
  element: Array<string> = [
    "/assets/icons/fire.svg",
    "/assets/icons/earth.svg",
    "/assets/icons/air.svg",
    "/assets/icons/water.svg"
  ];

  elementCounter: number = 0;
  currentElement = this.element[this.elementCounter];

  constructor() {}

  ngOnInit(): void {}

  leftArrow() {
    if (this.elementCounter === 0) {
      return (this.elementCounter = 3);
    }
    if (this.elementCounter >= 0) {
      this.elementCounter--;

      console.log(this.elementCounter);
    }
  }

  rightArrow() {
    if (this.elementCounter === 3) {
      return (this.elementCounter = 0);
    }
    if (this.elementCounter < 3) {
      this.elementCounter++;

      console.log(this.elementCounter);
    }
  }
}
