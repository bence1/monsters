import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { DataService } from "../data.service";

@Component({
  selector: "app-create",
  templateUrl: "./create.component.html",
  styleUrls: ["./create.component.css"]
})
export class CreateComponent implements OnInit {
  createForm: FormGroup;
  formStatus = false;

  constructor(private dataService: DataService) {}

  ngOnInit(): void {
    this.createForm = new FormGroup({
      name: new FormControl("", Validators.required),
      img: new FormControl("/assets/icons/fire.svg", Validators.required),
      attack: new FormControl("", Validators.required),
      defense: new FormControl("", Validators.required)
    });
  }

  onSubmit() {
    console.log(this.createForm.value);
    // Destructuring
    const { name, img, attack, defense } = this.createForm.value;
    this.dataService.createMonster(name, img, attack, defense);
    /* console.log(this.createForm.value); */
  }
}
