import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppComponent } from "./app.component";
import { CreateComponent } from "./create/create.component";
import { ElementalSelectorComponent } from "./create/elemental-selector/elemental-selector.component";
import { MonstersComponent } from "./monsters/monsters.component";
import { MonsterComponent } from "./monsters/monster/monster.component";
import { DataService } from "./data.service";
import { ReactiveFormsModule } from "@angular/forms";

@NgModule({
  declarations: [
    AppComponent,

    CreateComponent,
    ElementalSelectorComponent,
    MonstersComponent,
    MonsterComponent
  ],
  imports: [BrowserModule, ReactiveFormsModule],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule {}
