export interface Monster {
  name: string;
  img: string;
  attack: number;
  defense: number;
}
