import { Component, OnInit } from "@angular/core";
import { DataService } from "./data.service";
import { Monster } from "./monster.interface";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  title = "monsters";

  constructor(private dataService: DataService) {}
  ngOnInit() {}
}
